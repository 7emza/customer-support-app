<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\User;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        
        // //craete roles
        (Role::where('name', 'sys-admin')->first()) ?: Role::create(['name' => 'sys-admin']);
        (Role::where('name', 'admin')->first()) ?: Role::create(['name' => 'admin']);
        (Role::where('name', 'customer')->first()) ?: Role::create(['name' => 'customer']);

        // //create permissions
        (Permission::where('name', 'create')->first()) ?: Permission::create(['name' => 'create']);
        (Permission::where('name', 'display')->first()) ?: Permission::create(['name' => 'display']);
        (Permission::where('name', 'show')->first()) ?: Permission::create(['name' => 'show']);
        (Permission::where('name', 'edit')->first()) ?: Permission::create(['name' => 'edit']);
        (Permission::where('name', 'change status')->first()) ?: Permission::create(['name' => 'change status']);


        (User::where('email', "admin@easylens")->first()) ?: User::create([
            'name' => 'customer support',
            'email'    => 'admin@easylens',
            'email_verified_at' => now(),
            'password' => Hash::make('password'),
            'remember_token' => Str::random(10),
        ]);

        
        (User::where('email', "customer@easylens")->first()) ?: User::create([
            'name' => 'customer x',
            'email'    => 'customer@easylens',
            'email_verified_at' => now(),
            'password' => Hash::make('password'),
            'remember_token' => Str::random(10),
        ]);
        //customer 2
        (User::where('email', "customer2@easylens")->first()) ?: User::create([
            'name' => 'customer x',
            'email'    => 'customer2@easylens',
            'email_verified_at' => now(),
            'password' => Hash::make('password'),
            'remember_token' => Str::random(10),
        ]);

        //assing roles
        // User::where('email', '=', 'admin@easylens')->first()->assignRole('sys-admin');

        User::where('email', '=', 'admin@easylens')->first()->assignRole('admin');
        User::where('email', '=', 'customer@easylens')->first()->assignRole('customer');

        User::where('email', '=', 'hamza.driouch@uit.ac.ma')->first()->assignRole('sys-admin');
    }
}
